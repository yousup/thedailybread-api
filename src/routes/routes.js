"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const api_operations_1 = require("../helpers/api-operations");
var api_key = 'key-0a2da6960f82bc4c30a80d547f5a8339';
var domain = 'mail.edelshirestudios.com';
const mailgun = require("mailgun-js")({
    apiKey: api_key,
    domain: domain
});
class BaseRoute {
    constructor() {
        this.title = "theDailyBread";
        this.scripts = [];
    }
    addScript(src) {
        this.scripts.push(src);
        return this;
    }
    static create(router, model) {
        let operationLayer = new api_operations_1.Operation(model);
        console.log("[BaseRoute::create] Creating base route(s).");
        router.get("/home/:numLoaded", (req, res) => {
            console.log("[BaseRoute::create] Inside /" + req.params.amount + " GET method");
            operationLayer.homePage(req, res, req.params.numLoaded ? req.params.numLoaded : 0);
        });
        // This is for the admin app get all
        router.get("/getAll", (req, res) => {
            console.log("[BaseRoute::create] Inside /getAll GET method");
            operationLayer.getAll(req, res);
        });
        router.get("/travel/:amount", (req, res) => {
            console.log("[BaseRoute::create] Inside /travel/" + req.params.amount + " GET method");
            operationLayer.travelPage(req, res, req.params.amount);
        });
        router.get("/post/:postTitle", (req, res) => {
            console.log("[BaseRoute::create] Inside post/:postId");
            operationLayer.getPost(req, res, req.params.postTitle);
        });
        router.post("/email", (req, res, next) => {
            console.log("[BaseRoute::create] Inside /email POST method");
            let data = {
                from: req.body.email,
                to: "yousup@wyleeimagery.com",
                subject: req.body.subject,
                text: req.body.message,
                "h:Reply-To": req.body.email
            };
            mailgun.messages().send(data, (error, body) => {
                if (error) {
                    console.log(error);
                    res.json({ statusMessage: "There was an error sending your email", status: "failed" });
                }
                else {
                    res.json({ statusMessage: "Your message was sent successfully!", status: "sent" });
                }
            });
        });
        router.post("/newpost", (req, res) => {
            console.log("[BaseRoute::create] Inside /newpost");
            operationLayer.publishNewPost(req, res);
        });
        router.post("/delete", (req, res) => {
            console.log("[BaseRoute::create] Inside /delete");
            operationLayer.deletePost(req, res);
        })
        router.patch("/update/:id", (req, res) => {
            console.log("/update/" + req.params.id);
            operationLayer.updatePost(req, res, req.params.id);
        })
    }
    home(req, res) {
        this.title = "theDailyBread | Home";
        let options = {
            "message": "theDailyBread",
        };
    }
}
exports.BaseRoute = BaseRoute;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.postSchema = new mongoose_1.Schema({
    createdAt: Date,
    category: { type: String, enum: ["travel", "gear review", "atx", "lab", "life"] },
    postTitle: String,
    postBody: String,
    videoUrl: String,
    photoUrl: String,
    tagline: String,
    otherUrls: []
});
exports.postSchema.pre("save", (next) => {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    if (!this.category) {
        this.category = "life";
    }
    next();
});

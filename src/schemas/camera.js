"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.cameraSchema = new mongoose_1.Schema({
    createdAt: Date,
    make: String,
    cameraModel: String,
    price: String,
    type: { type: String, enum: ['fullFrame'] },
    sensorSize: { type: String, enum: ['ilc'] }
});
exports.cameraSchema.pre("save", (next) => {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sensorSize;
(function (sensorSize) {
    sensorSize[sensorSize["mediumFormat"] = 0] = "mediumFormat";
    sensorSize[sensorSize["fullFrame"] = 1] = "fullFrame";
    sensorSize[sensorSize["apsc"] = 2] = "apsc";
    sensorSize[sensorSize["microFourThirds"] = 3] = "microFourThirds";
    sensorSize[sensorSize["oneInch"] = 4] = "oneInch";
})(sensorSize = exports.sensorSize || (exports.sensorSize = {}));
var cameraType;
(function (cameraType) {
    cameraType[cameraType["ilc"] = 0] = "ilc";
    cameraType[cameraType["fixedLens"] = 1] = "fixedLens";
})(cameraType = exports.cameraType || (exports.cameraType = {}));

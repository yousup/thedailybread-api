// in mongo shell: load("life-posts.js")
// mongo ds117469.mlab.com:17469/thedailybread-dev -u <dbuser> -p <dbpassword>
var bulk = db.posts.initializeOrderedBulkOp();
bulk.find({"postTitle": "And God said, \“Let the waters swarm with swarms of living creatures\""}).upsert().updateOne(
	{
		category: "travel",
		postTitle: "And God said, \“Let the waters swarm with swarms of living creatures\"",
		postBody: `You know, during my excursion to the Great Barrier Reef, I actually had a GoPro (or rather a knock-off of one) <em>and</em> an underwater case for it, <em>but I didn't take it with me scuba diving because I thought it would be too deep</em>. Turns out, it wasn't. So now I have to do my best to recreate the experience through mere words. Bear with me.
		
		You may or may not have read my previous post about my time in New Zealand walking the Milford Track, but this excursion to the Great Barrier Reef (henceforth known as GBR) took place during the same trip to that same corner of the planet, and frankly, it was the only real reason why I decided to stop by Australia on the way to New Zealand. I wasn't too sure how hyped I was supposed to be, but one thing was unequivocal: for two years leading up to this moment, I had paid <em>regular</em> visits to the Wild Reef exhibit over at Chicago's Shedd Aquarium, and if dunking in the GBR would translate to an \"immersive version\" of that exhibit, then I <em>absolutely</em> ought to have been hyped.

		The designated day was Thanksgiving Day, 2016. While most of my countrymen would prepare for a day inundated with turkey and gravy, Australians practiced no such ritual, and in the town of Cairns, Queensland, summer was to dawn shortly with the temperatures already starting to flirt with the upper 30s (in Celsius of course). And on a cloudless day, the conditions were flawless for a dip into the richest marine habitat the world has ever known, probably.

		The day began at around 7:00AM when I left my hostel (YHA Cairns, quite a nice hostel actually) to trot over to the pier where all the different tour operators' catamarans were docked, on standby to shuttle heaps of tourists on their aquatic adventure-to-be, me included. The operator that I chose was Reef Magic, and it was a fairly painless process to get myself checked in and boarded for the journey to the platform (some operators conduct their tours from the boat itself, and others like Reef Magic take you to a platform out next to the reef). 

		There were two notable observations that I made on that catamaran ride: the first was that I realized pretty quickly that I was the only one traveling solo that day, which put a slight damper on my spirits along with an activated drive to make some friends. The second was that for the first time in my life (and to date, the only time) I experienced sea sickness! I tried enacting various strategies to dispel it to no avail, but thankfully we reached our destination, the Marine World Activity Platform, before any vomiting sensations overwhelmed me.
		
		Marine World was a surprisingly homely platform that served as a good base for a variety of aquatic activities like semi-submersible tours, scuba diving, snorkeling, and even scenic helicopter flights. As I took in the surroundings, I nearly forgot that I needed to make some friends, so I struck up a talk with two girls from Seattle (shoutout to Madi and Kelsie!) whom I met during the scuba diving crash-course on the catamaran ride and they graciously agreed to be my American compatriots for the day. 

		Up to this point in my life I had never gone scuba diving nor had I even really gone snorkelling, at least not anywhere memorable. I truly did try to capture the gem that is the GBR with my  inadequate GoPro knockoff, I really did. From the rainbow array of healthy coral to the largest of fish (the Humphead Wrasse. Look it up), I suddenly had a flashback of myself standing in front of the reef exhibit back at Shedd, right as the aquarium opened, by myself, trying to take in as much as I could from behind the glass panel. Now 8,800 miles from home, the panel was removed, and there I was, swimming amongst the butterflyfish and through colonies of moon jellies. And that was just the snorkelling.

		The first beginner's scuba session was restrictive in that we were all arm-linked with the divemaster. We had a photo shoot with a photographer about 10 feet below the surface, and I was lucky enough for the resident Humphead Wrasse (seriously, look it up. It's a massive hunk of a fish) to sneak in under my armpit right as my photo was taken. Naturally I overpaid for that print. Where things got interesting was during the second dive, which I took with Kelsie since Madi wasn't very comfortable with the idea, and this time the divemaster let us loose (under close supervision of course) more than 30 feet below the surface. Was I expecting to pick up a live, squishy sea cucumber chilling on the sea floor? Heck no. On top of that, could I believe my eyes that I was perhaps 4 inches away from a family of clownfish (legit might have been Nemo and Marvin) darting in and out of their host anemone? Or just a few minutes later, that I would <em>pet</em> a 5-foot long Green Sea Turtle (legit might have been Crush) pecking away at delicious(?) coral and otherwise completely ignoring us? I was lucky enough I didn't explode from the resurrected excitement of my 5-year old giddy self. Afterwards as soon as our mouthpieces were off, I'm pretty sure I remember Kelsie and I blurted simultaneously out to each other "THAT WAS AWESOME". Creation needed no further explanation.

		The rest of the time on Marine World was spent just dumping icing on the cake. Had lunch, went on the semi-submersible, and flew my drone over the vast glistening ecosystem (one of the workers earlier had a bad altercation with a Chinese tourist, and thinking I was also one started gesturing aggressively to me along with purposely broken English for me to stop flying the drone. It was rather funny. I deflated the situation quickly). Eventually, it was time to say good bye to Marine World and the Great Barrier Reef. I did not get sea sick on the journey back to shore.
		
		I spent the rest of that evening hanging out with Kelsie and Madi. We talked about our travels and our lives back home in the US while sipping on sangria and sharing a seafood Thanksgiving meal, all to cap off what is easily, to this day, one of the top five experiences we've ever had (I'm going to speak for them too, confidently). I think back to that day every once in a while, what it would have been like if I had taken my GoPro thing on that second dive, or how differently I would have made this video if I hadn't just started my storytelling career at the time. But mostly, I just think back to how awesome it all was.
		`,
		createdAt: "05/05/2018 GMT-6:00",
		tagline: "I truly do believe that everyone on this earth has some sort of a passion for the created world. For some it is astronomy and the legions of the stars. For others, it might be the studies of climate, or geology, or the vegetation that covers the earth. For me, nothing fascinates me more than the obsession with what lurks beneath our oceans, and for the first time in my life lay an opportunity to interact with that mystery, face to face.",
		videoUrl: "aCNa3iEFqjA",
	});
bulk.find({"postTitle": "Another new beginning"}).upsert().updateOne(
	{
		category: "life",
		postTitle: "Another new beginning",
		postBody: `The month of November 2016 marked the start of a crazy journey for me; I had accepted a voluntary severance package from my old job, and with it I set out to journey around the world. \"Crazy adventure\" would be an understatement. In fact, there is perhaps no single statement that can quite capture the behemoth that was what I now dub the <em>cognizance</em> year: cognizance about who I am, what I am looking for in this vast, vast life, but also what I am <em>not</em> looking for.

		But if there was any one thing that I've definitively figured out during that year was my desire to further pursue the art of storytelling. This website shall serve as a platform to do so (and honestly, having built this website from scratch it is itself a story to tell) and will over time act as a central place to compile all of my \"works\" if you will. The video above, along with the few posts that I've already published, will hint at what sorts of content are to come (hint: there will be a lot of content about \"doing life\", and not just my life. Technology will also be a staple feature). Don't hesitate to subscribe!`,
		createdAt: "04/28/2018 GMT-06:00",
		tagline: "Thoughts on the reasoning for starting this new platform.",
		videoUrl: "KGsp-O14ty4",
	});
bulk.find({"postTitle": "Objectively the most important day in history"}).upsert().updateOne(
	{
		category: "life",
		postTitle: "Objectively the most important day in history",
		postBody: `Easter is a precious time for me. At one point in college, the relatively unbounded freedom afforded to me led to disastrous results in my personal life, results that ultimately landed me face to face with my humanity. My own raw, destitute, and naive humanity, sure to lead me to the deepest spiritual abyss, spiritual death. 

		But thanks be to God, as Jay mentions in this video that with the resurrection from the tomb, Christ has defeated this death and releases us from the fear of it, the fear of where, when unbounded, we will take ourselves. That's why kids can have egg hunts and we can commune together with laughter. This is the better way.`,
		createdAt: "04/09/2018 GMT-06:00",
		tagline: "Thoughts on Easter Sunday and why it is the most paramount day of all time.",
		videoUrl: "bhV_eJpqT3c",
	});
bulk.find({"postTitle": "Adventures in Nikon: New Zealand"}).upsert().updateOne(
	{
		category: "travel",
		postTitle: "Adventures in Nikon: New Zealand",
		postBody: `For as long as memory allows, I could never really have a thought of Australia without also thinking of New Zealand. A mysticism of sorts has always accompanied my perceptions of that island nation, a mysticism not meant to be unshrouded, but only to be experienced, and to be departed from as it was. It only remained to be visited.
		
		I had treated the other chapters of that trip, time spent in both Japan and Australia, as processions up to my New Zealand experience. I had alotted careful space in my visions of the overall trip for New Zealand, and more specifically, the Great Walk known as the Milford Track. So with my Nikon D750 equipped with a 20mm f/2.8D (this video is filmed primarily with this gear), I dove in to my first, true amateur experience as a documenter of the backcountry, the untamed.
		
		Fiordland National Park, the setting for the Milford Track, is one of the wettest places on earth, receiving up to 8000mm (315 in.!) of precipitation annually. This, combined with the surrounding oceanic climate gifts us with one of the most beautiful, temperate rainforests on our planet. Shrouded in mist by morning, and (in our lucky case) brilliant sunsets by dusk, my time on the Milford Track did nothing to demystify Middle Earth for me. Rather, as I had hoped, it served to only increase the sense of mystery tucked away in this corner of the world.`,
		createdAt: "03/31/2018 GMT-06:00",
		tagline: "Remembering the early days of mixing cameras and travel amidst the natural elements",
		videoUrl: "OY9uE4YF6Wg",
	});
bulk.execute();
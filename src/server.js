"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const express = require("express");
const logger = require("morgan");
const path = require("path");
const cors = require("cors");
const errorHandler = require("errorhandler");
const methodOverride = require("method-override");
const mongoose = require("mongoose");
const routes_1 = require("./routes/routes");
const camera_1 = require("./schemas/camera");
const post_1 = require("./schemas/post");
class Server {
    static bootstrap() {
        return new Server();
    }
    constructor() {
        this.model = {};
        this.app = express();
        this.config();
        this.routes();
        this.api();
    }
    api() {
    }
    config() {
        const MONGODB_DEV = "mongodb://yousuplee:password@ds117469.mlab.com:17469/thedailybread-dev";
        global.Promise = require("q").Promise;
        mongoose.Promise = global.Promise;
        let connection = mongoose.createConnection(MONGODB_DEV);
        this.model.camera = connection.model("Camera", camera_1.cameraSchema);
        this.model.post = connection.model("Post", post_1.postSchema);
        // enable cors
        this.app.use(cors());
        this.app.use(express.static(path.join(__dirname, "public")));
        this.app.set("views", path.join(__dirname, "views"));
        this.app.set("view engine", "pug");
        this.app.use(logger("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        this.app.use(cookieParser("SECRET_GOES_HERE"));
        this.app.use(methodOverride());
        this.app.use(function (err, req, res, next) {
            err.status = 404;
            next(err);
        });
        this.app.use(errorHandler());
    }
    routes() {
        let router;
        router = express.Router();
        routes_1.BaseRoute.create(router, this.model);
        this.app.use('/home', router);
        this.app.use(router);
    }
}
exports.Server = Server;

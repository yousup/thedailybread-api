"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Operation {
    constructor(model) {
        this.model = model;
    }
    homePage(req, res, numTimesAlreadyLoaded) {
        let limit = 7;
        const Post = this.model.post;
        Post.find()
            .limit(limit)
            .skip(parseInt(numTimesAlreadyLoaded) * limit)
            .sort("-createdAt")
            .exec(function (err, data) {
            res.json({
                tabTitle: "theDailyBread | home",
                pageTitle: "theDailyBread",
                posts: data,
            });
        });
    }
    // getAll is used only by the admin app
    getAll(req, res) {
        // Figure out how to paginate this in the future
        const Post = this.model.post;
        Post.find({}, 'postTitle createdAt photoUrl videoUrl tagline postBody')
        .sort("-createdAt")
        .exec((err, docs) => {
            if (err) {
                console.log(err)
                res.json({
                    error: err
                });
            }
            else {
                res.json({
                    posts: docs
                });
            }
        })
    }
    travelPage(req, res, numTimesAlreadyLoaded) {
        let limit = 7;
        const Post = this.model.post;
        Post.find({ 'category': 'travel' })
            .limit(limit)
            .skip(parseInt(numTimesAlreadyLoaded) * limit)
            .sort("-createdAt")
            .exec(function (err, data) {
            res.json({
                tabTitle: "theDailyBread | travel",
                pageTitle: "theDailyBread",
                posts: data,
            });
        });
    }
    getPost(req, res, postTitle) {
        const Post = this.model.post;
        Post.findOne({ 'postTitle': postTitle.replace(RegExp("-", "g"), " ") })
            .exec(function (err, data) {
            res.json({
                tabTitle: data.postTitle,
                pageTitle: "theDailyBread",
                post: data
            });
        });
    }

    publishNewPost(req, res) {
        const Post = this.model.post;
        let data = {};
        req.body.videoOrPhoto == "photo" ? data["photoUrl"] = req.body.mediaURL : data["videoUrl"] = req.body.mediaURL;
        data['postTitle'] = req.body.title;
        data['tagline'] = req.body.tagline;
        data['postBody'] = req.body.body;
        data['createdAt'] = req.body.createdAt;
        data['category'] = "life";
        console.log(data);
        Post.create(data, (err) => {
            if (err) {
                console.log(err);
                res.json({ statusMessage: "There was an error creating the post", status: "failed", reason: err });
            }
            else {
                console.log("success");
                res.json({ statusMessage: "Post created successfully!", status: "success" });
            }
        })
    }

    deletePost(req, res) {
        const Post = this.model.post;
        Post.remove({_id: req.body.id}, (err) => {
            if (err) {
                console.log(err);
                res.json({ statusMessage: "There was an error deleting the post", status: "failed", reason: err });
            }
            else {
                console.log("deleted");
                res.json({ statusMessage: "Post deleted successfully", status: "success" });
            }
        });
    }

    updatePost(req, res, id) {
        const Post = this.model.post;
        let data = {};
        req.body.videoOrPhoto == "photo" ? data["photoUrl"] = req.body.mediaURL : data["videoUrl"] = req.body.mediaURL;
        data['postTitle'] = req.body.title;
        data['tagline'] = req.body.tagline;
        data['postBody'] = req.body.body;
        data['createdAt'] = req.body.date;
        Post.updateOne(
            {_id: req.body.id},
            data,
            {omitUndefined: true},
            (err) => {
                if (err) {
                    console.log(err);
                    res.json({ statusMessage: "There was an error updating the post", status: "failed", reason: err });
                }
                else {
                    console.log("success");
                    res.json({ statusMessage: "Post updated successfully", status: "success" });
                }
            }
        )
    }
}
exports.Operation = Operation;
